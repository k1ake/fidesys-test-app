import config
import os
import subprocess
import base64

# VTK
import vtk
from vtk.util.numpy_support import vtk_to_numpy


def check_folder():
    if not os.path.isdir("./done/"):
        os.mkdir("./done/")


def solve(fc_file: str):
    calc_path = os.path.join(
        os.path.expanduser(config.FIDESYS_DIR), "calc", "bin", "FidesysCalc"
    )
    pvd_file = "./done/" + fc_file.split(".")[0] + ".pvd"
    subprocess.call(
        [
            calc_path,
            "--input=" + os.path.abspath(fc_file),
            "--output=" + os.path.abspath(pvd_file),
        ]
    )


def read_vtk(filename: str):
    reader = vtk.vtkXMLUnstructuredGridReader()
    filename = os.path.abspath(f"./done/{filename}/case1_step0001_substep0001.vtu")
    reader.SetFileName(filename)
    reader.Update()
    grid = reader.GetOutput()
    point_data = grid.GetPointData()

    arrayOfStress = vtk_to_numpy(point_data.GetArray("Stress"))
    # id = 6 - Напряжения по Мизесу
    print(arrayOfStress, "\n\n")
    mises = [abs(i[5]) for i in arrayOfStress]
    print(mises)
    print(max(mises), end="")


if __name__ == "__main__":
    # check_folder()
    # solve("tmp.fc")
    read_vtk("tmp")
