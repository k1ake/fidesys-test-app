import vtk
import os
from vtk.util.numpy_support import vtk_to_numpy


def read_vtk():
    reader = vtk.vtkXMLUnstructuredGridReader()
    filename = os.path.abspath("./results/tmp/case1_step0001_substep0001.vtu")
    reader.SetFileName(filename)
    reader.Update()
    grid = reader.GetOutput()
    point_data = grid.GetPointData()

    arrayOfStress = vtk_to_numpy(point_data.GetArray("Stress"))
    # id = 6 - Напряжения по Мизесу
    mises = [abs(i[6]) for i in arrayOfStress]
    print(max(mises), end="")


if __name__ != "__main__":
    raise SystemExit("This should be run as main script")

else:
    read_vtk()
