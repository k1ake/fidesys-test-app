#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMap>

QT_BEGIN_NAMESPACE
namespace Ui
{
    class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    QString beam_properties();
    bool write_fc();
    ~Widget();
    bool start_Calc();

private slots:
    void on_calc_btn_clicked();

    void on_choose_btn_clicked();

private:
    Ui::Widget *ui;
    QMap<QString, double> i_beam_values;
    double power;
    QString path_to_Fidesys;
};
#endif // WIDGET_H
