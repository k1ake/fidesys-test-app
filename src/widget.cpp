// Libs for Interface
#include "widget.h"
#include "./ui_widget.h"
#include <QFileDialog>

// Working with Files
#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <QPixmap>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

// For launching FidesysCalc
#include <QProcess>

Widget::Widget(QWidget *parent)
    : QWidget(parent), ui(new Ui::Widget)
{
    ui->setupUi(this);
    // Setting some path to Fidesys
    path_to_Fidesys = QString(getenv("HOME")) + "/CAE-Fidesys-6.0";
    // I №10 parametres
    i_beam_values.insert("B1", 0.055);
    i_beam_values.insert("B2", 0.055);
    i_beam_values.insert("c1", 0.0072);
    i_beam_values.insert("c2", 0.0072);
    i_beam_values.insert("d", 0.0045);
    i_beam_values.insert("H", 0.1);
    power = 1000;

    // Displaying default parametres
    ui->b1_dsb->setValue(i_beam_values.value("B1"));
    ui->b2_dsb->setValue(i_beam_values.value("B2"));
    ui->c1_dsb->setValue(i_beam_values.value("c1"));
    ui->c2_dsb->setValue(i_beam_values.value("c2"));
    ui->d_dsb->setValue(i_beam_values.value("d"));
    ui->h_dsb->setValue(i_beam_values.value("H"));
    ui->p_sb->setValue(power);

    QPixmap ibeam_pic("./I-Beam.png"), loads_pic("./loads.png");
    ui->IBeamImg->setPixmap(ibeam_pic);
    ui->BeamLoadImg->setPixmap(loads_pic);
}

Widget::~Widget()
{
    qInfo() << "Exiting..";
    delete ui;
}

/**
 * @brief Sleep implementation, needed for changing labels between functions
 * @param msec - time in msec to wait
 */
void delay(int msec)
{
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

/**
 * @brief Writes all data recieved from UI to "tmp.fc"
 * @return true if succeed else false
 */

bool Widget::write_fc()
{
    QFile beam_fc("beam.fc"), tmp_fc("tmp.fc");
    if (!beam_fc.open(QIODevice::ReadOnly))
    {
        qCritical() << "Can't open beam.fc";
        return false;
    }
    QJsonDocument beam_json(QJsonDocument::fromJson(beam_fc.readAll()));
    QJsonObject json_obj = beam_json.object();

    // Recreating part of json to change force value
    QJsonArray loads, data;
    QVariantMap load;
    load.insert("apply_to", json_obj["loads"][0]["apply_to"]);
    load.insert("apply_to_size", json_obj["loads"][0]["apply_to_size"]);
    load.insert("cs", json_obj["loads"][0]["cs"]);
    load.insert("data", json_obj["loads"][0]["data"]);
    load.insert("dependency_type", json_obj["loads"][0]["dependency_type"]);
    load.insert("id", json_obj["loads"][0]["id"]);
    load.insert("name", json_obj["loads"][0]["name"]);
    load.insert("type", json_obj["loads"][0]["type"]);

    for (int i = 0; i < 6; i++)
    {
        QJsonArray *tmp = new QJsonArray;
        if (i == 2)
            tmp->append(float(-power));
        else
            tmp->append(float(0.0));
        data.append(*tmp);
    }
    load["data"] = data;
    loads.append(QJsonObject::fromVariantMap(load));

    json_obj["loads"] = loads;

    // Recreating part of json to change I-beam properties
    QJsonArray property_tables;
    QVariantMap properties_1, geometry, inner_properties_1;
    properties_1.insert("id", json_obj["property_tables"][0]["id"]);
    properties_1.insert("type", json_obj["property_tables"][0]["type"]);

    geometry.insert("B1", i_beam_values["B1"]);
    geometry.insert("B2", i_beam_values["B2"]);
    geometry.insert("H", i_beam_values["H"]);
    geometry.insert("c1", i_beam_values["c1"]);
    geometry.insert("c2", i_beam_values["c2"]);
    geometry.insert("d", i_beam_values["d"]);

    inner_properties_1.insert("I-Beam", json_obj["property_tables"][0]["properties"]["I-Beam"]);
    inner_properties_1.insert("angle", json_obj["property_tables"][0]["properties"]["angle"]);
    inner_properties_1.insert("ey", json_obj["property_tables"][0]["properties"]["ey"]);
    inner_properties_1.insert("ez", json_obj["property_tables"][0]["properties"]["ez"]);
    inner_properties_1.insert("mesh_quality", json_obj["property_tables"][0]["properties"]["mesh_quality"]);
    inner_properties_1.insert("section_type", json_obj["property_tables"][0]["properties"]["section_type"]);
    inner_properties_1.insert("warping_dof", json_obj["property_tables"][0]["properties"]["warping_dof"]);
    inner_properties_1.insert("geometry", geometry);
    properties_1["properties"] = inner_properties_1;

    property_tables.push_back(QJsonObject::fromVariantMap(properties_1));
    json_obj["property_tables"] = property_tables;

    if (!tmp_fc.open(QIODevice::WriteOnly))
    {
        qCritical() << "Can't write new .fc file";
        return false;
    }
    tmp_fc.write(QJsonDocument(json_obj).toJson(QJsonDocument::Indented));

    beam_fc.close();
    tmp_fc.close();
    return true;
}

/**
 * @brief Finds FidesysCalc, then starts calculations using it
 * @return true if succeed else false
 */

bool Widget::start_Calc()
{
    QString path_to_Calc = path_to_Fidesys;

    if (QSysInfo::kernelType() != "linux")
    {
        path_to_Calc += "/bin/FidesysCalc.exe";
    }
    else
        path_to_Calc += "/calc/bin/FidesysCalc";

    qInfo() << "Path to FidesysCalc:" << path_to_Calc;
    if (!QFile(path_to_Calc).exists())
    {
        qCritical() << "FidesysCalc not found!";
        ui->results_label->setText("No FidesysCalc found");
        return false;
    }

    if (!QDir(QDir::currentPath().append("results")).exists())
    {
        QDir().mkdir("results");
    }

    QMap<QString, QByteArray> calc_output;

    QProcess *process = new QProcess();
    QStringList calc_args;
    calc_args << "--input=" + QDir::currentPath() + "/tmp.fc"
              << "--output=" + QDir::currentPath() + "/results/tmp.pvd";
    qInfo() << "Starting process" << path_to_Calc << calc_args[0] << calc_args[1];

    process->setProgram(path_to_Calc);
    process->setArguments(calc_args);
    process->start();
    process->waitForStarted(-1);
    qInfo() << "Calculation started";
    process->waitForFinished(-1);

    calc_output["errors"] = process->readAllStandardError();
    calc_output["output"] = process->readAllStandardOutput();
    qInfo() << "Errors:" << QString(calc_output["errors"]);
    qInfo() << "Calc output" << QString(calc_output["output"]);
    if (QString(calc_output["errors"]) != "")
    {
        ui->results_label->setText(calc_output["errors"]);
        return false;
    }

    qInfo() << "!!DONE!!";
    return true;
}

/**
 * @brief Passes created .vtu file to python script to find stress values
 * @return maximal stress from beam nodes. -1 if there are errors with parsing
 */
double python_get_stress()
{
    QProcess *process = new QProcess();
    QStringList process_args;
    process_args << "vtk_stress.py";
    process->setProgram("python");
    process->setArguments(process_args);
    process->start();
    process->waitForStarted(-1);
    qInfo() << "Python started";
    process->waitForFinished(-1);
    qInfo() << "Done";

    QMap<QString, QByteArray> python_output;
    python_output["errors"] = process->readAllStandardError();
    python_output["output"] = process->readAllStandardOutput();
    qInfo() << "Python script results:";
    qInfo() << "Errors:" << QString(python_output["errors"]);
    qInfo() << "Output" << QString(python_output["output"]);

    if (python_output["errors"].size())
        return -1;
    return QString(python_output["output"]).toDouble();
}

/**
 * @brief Recieves all data from input boxes, then writes .fc file, passes it to FidesysCalc and update results_label with max stress and conclusion on beam's strength
 */
void Widget::on_calc_btn_clicked()
{
    // Updating beam and force properties
    i_beam_values["B1"] = ui->b1_dsb->value();
    i_beam_values["B2"] = ui->b2_dsb->value();
    i_beam_values["c1"] = ui->c1_dsb->value();
    i_beam_values["c2"] = ui->c2_dsb->value();
    i_beam_values["d"] = ui->d_dsb->value();
    i_beam_values["H"] = ui->h_dsb->value();
    power = ui->p_sb->value();

    // Preparing data for FidesysCalc
    if (!write_fc())
    {
        ui->results_label->setText("Errors with writing new beam properties");
        return;
    }

    ui->results_label->setText("Calculation started");

    // Delay needed otherwise no message will be displayed
    delay(1);
    if (!start_Calc())
        return;

    double MAX_STRESS = 200000000, SAFETY_RATIO = 1.3;
    double maximal_stress = python_get_stress();

    qInfo() << "Maximal stress:" << maximal_stress;

    QString base_answer = "Max Stress = " + QString::number(maximal_stress) + " -> ";
    if (maximal_stress < 0)
        ui->results_label->setText("Errors with parsing results");
    else if (maximal_stress > MAX_STRESS / SAFETY_RATIO)
    {
        ui->results_label->setText(base_answer + "strength is NOT enough");
    }
    else
    {
        ui->results_label->setText(base_answer + "strength is enough");
    }
}

/**
 * @brief Launches system's file chooser to pick path to CAE-Fidesys-X.X dir
 */
void Widget::on_choose_btn_clicked()
{
    path_to_Fidesys = QFileDialog::getExistingDirectory();
    // 54 is the max length of string that doesn't shrink the button
    QString output = path_to_Fidesys.length() > 54 ? path_to_Fidesys.mid(0, 51) + "..." : path_to_Fidesys;
    ui->path_label->setText(output);
}
