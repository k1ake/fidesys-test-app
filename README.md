# Fidesys test app
Тестовое задание для Fidesys, показывающее навыки работы с qt и vtk

## Задание:
Сделать программку с графическим интерфейсом на основе Qt, которая будет запускать Фидесис на счёт, читать результаты и на их основе выдавать какое-то заключение
Чтение результатов - это VTK

## Подробности:
У ядра есть входной файл .fc, который генерируется препроцессором
Выходным является .vtu файл с внутрянкой vtk

## Сборка (linux):
- Cоздать папку buid и перейти в неё: `mkdir build && cd build`
- Сконфигурировать файлы сборки Cmake: `cmake ../src`
- Скомпилировать: `make`

## Сборка (windows):
- Вроде должно точно также собираться, проверить не могу

## Зависимости
### C+++:
    qt6-devel
### Python
    vtk
## Скриншот
!![preview](screenshot.png)

## Ссылки: 
- [Примеры Python api](https://fidesys-solvers.ru/analysis/fidesys-python-api)
- [Qt Creator документация](https://wiki.qt.io/Qt_Creator_Guidance)
- [Пример связки Qt-VTK](https://github.com/martijnkoopman/Qt-VTK-viewer)


## Не работает:
- Подтягивание VTK с помошью CMake при билде в помощью Qt Creator (Нативный не работает, а в флэтпаке не подхватывает по очевидным причинам)

<!-- https://stackoverflow.com/questions/35897655/vtk-extracting-cell-data-from-vtu-unstructured-grids-in-c -->